<?php

header("Content-type: text/html; charset=utf-8");
error_reporting(-1);
require_once 'connect.php';//подключаем файл для подключения к базе данных
require_once 'funcs.php';//подключаем файл с функциями
if(!empty($_POST)){//если массив $_POST не пуст
	save_mess();//вызыв ф-ию котрая из ключей массива создаёт переменные с их значениями и их экрон
	header("Location: {$_SERVER['PHP_SELF']}");//делаем редирект на этот же файл(устр.проблемму f5)
	exit;
}

$messages = get_mess();// вызываем ф-ию кторая помещает данные из таблицы в асоциаотивный массив
// print_arr($messages);

?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Гостевая книга</title>


<style>

	.message{
		border: 1px solid #ccc;
		padding: 10px;
		margin-bottom: 20px;
	}

</style>

</head>
<body>

	<form action="index.php" method="post">
		<p>
			<label for="name">Имя:</label><br>
			<input type="text" name="name" id="name">
		</p>
		<p>
			<label for="text">Текст:</label><br>
			<textarea name="text" id="text"></textarea>
		</p>
		<button type="submit">Написать</button>
	</form>

	<hr>

	<?php if(!empty($messages)): //если масив ($messages) не пуст?>
		<?php foreach($messages as $key)://проходим по этому массиву циклом ?>
			<div class="message"><!--<div> для стилилизации-->
				<p>Автор: <?=$key['name']?> | Дата: <?=$key['date']?></p>
				<div><?=nl2br(htmlspecialchars($key['text']))//что б тэги ведён в текст не обрабатывались?></div>
			</div>
		<?php endforeach; ?>
	<?php endif; ?>
	<a href="\index.php">Страница INDEX.PHP</a>
</body>
</html>